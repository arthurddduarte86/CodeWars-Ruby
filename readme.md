# Readme com informações e catálogo do meu processo na plataforma CodeWars para Ruby

## Sobre
Sejam bem vindos, este é o catálogo de exercícios e códigos para a linguagem Ruby na plataforma CodeWars.

# Kata
## Rank:8 kyu

* `Multiply` ||  [  multiply.rb  ](https://github.com/arthurddduarte86/CodeWars-Ruby/blob/main/Code-Rb/Multiply.rb)  Site [CodeWars, rb  ](https://www.codewars.com/kata/50654ddff44f800200000004/train/ruby)
* `Sentence Smash` ||  [  Sentence_Smash.rb  ](https://github.com/arthurddduarte86/CodeWars-Ruby/blob/main/Code-Rb/Sentence_Smash.rb)  Site [CodeWars, rb  ](https://www.codewars.com/kata/53dc23c68a0c93699800041d/train/ruby)
* `Reversed Strings` ||  [  Reversed_Strings.rb  ](https://github.com/arthurddduarte86/CodeWars-Ruby/blob/main/Code-Rb/Reversed_Strings.rb)  Site [CodeWars, rb  ](https://www.codewars.com/kata/5168bb5dfe9a00b126000018/train/ruby)
* `How good are you really?` ||  [  How_good_are_you_really.rb  ](https://github.com/arthurddduarte86/CodeWars-Ruby/blob/main/Code-Rb/How_good_are_you_really.rb)  Site [CodeWars, rb  ](https://www.codewars.com/kata/5601409514fc93442500010b/train/ruby)
* `Sum of positive` ||  [  Sum_of_positive.rb  ](https://github.com/arthurddduarte86/CodeWars-Ruby/blob/main/Code-Rb/Sum_of_Positive.rb)  Site [CodeWars, rb  ](https://www.codewars.com/kata/5715eaedb436cf5606000381/train/ruby)
* `Hello World` ||  [  Hello-World.rb  ](https://github.com/arthurddduarte86/CodeWars-Ruby/blob/main/Code-Rb/Hello-World.rb)  Site [CodeWars, rb  ](https://www.codewars.com/kata/523b4ff7adca849afe000035/train/ruby)